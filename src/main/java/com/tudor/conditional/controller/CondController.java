package com.tudor.conditional.controller;

import com.tudor.conditional.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller("/home")
public class CondController {
    @Autowired
    IService iService;

    @GetMapping
    public ResponseEntity home() {
        return ResponseEntity.ok("this is home ENABLED = " + iService.getValue());
    }
}

package com.tudor.conditional.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(value = "feature1.enabled", havingValue = "false", matchIfMissing = false)
public class DisabledService extends IService {
    public String getValue() {
        return "feature DISABLED";
    }
}

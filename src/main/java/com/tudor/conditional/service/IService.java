package com.tudor.conditional.service;

import org.springframework.stereotype.Component;

@Component
public abstract class IService {
    public abstract String getValue();
}
